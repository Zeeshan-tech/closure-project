function counterFactory(){
    let value=0;
    return {
        increment (){
          value++;
        },
        decrement(){
          value--
        },
        getValue(){
            return value
        }
    }
}

module.exports=counterFactory